package main

import (
	"context"
	"database/sql"
	"fmt"
)

type Sql interface {
	*sql.DB | *sql.Tx
}

type Payout struct {
	ID string

	Amount   int64
	Currency string
}

type PayoutRepository[T Sql] struct {
	sql T
}

type Txer interface {
	BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error)
}

type PayoutTxBlock func(context.Context, *PayoutRepository[*sql.Tx]) error

func (r *PayoutRepository[Sql]) Txed(
	ctx context.Context,
	block PayoutTxBlock,
) error {
	txer, ok := any(r.sql).(Txer)
	if !ok {
		return fmt.Errorf("%T does not implement Txer", r.sql)
	}

	tx, err := txer.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("begin: %w", err)
	}

	pr := PayoutRepository[*sql.Tx]{
		sql: tx,
	}

	if err := block(ctx, &pr); err != nil {
		tx.Rollback() // nolint: errcheck
		return err
	}

	return tx.Commit()
}

type RowQueryerContext interface {
	QueryRowContext(ctx context.Context, query string, args ...any) *sql.Row
}

func (r *PayoutRepository[Sql]) Store(ctx context.Context, p *Payout) error {
	queryer, ok := any(r.sql).(RowQueryerContext)
	if !ok {
		return fmt.Errorf("%T does not implement RowQueryerContext", r.sql)
	}

	if err := queryer.QueryRowContext(
		ctx,
		"INSERT INTO payouts (amount, currency) VALUES ($1,$2) RETURNING id",
		p.Amount,
		p.Currency,
	).Scan(
		&p.ID,
	); err != nil {
		return fmt.Errorf("queryer context: %w", err)
	}

	return nil
}
