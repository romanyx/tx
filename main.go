package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"

	"github.com/ory/dockertest/v3"
)

func main() {
	// Database setup.

	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("create new pool: %v", err)
	}

	pgDocker := prepareDB(pool)
	defer func() {
		pgDocker.DB.Close()
		pool.Purge(pgDocker.Resource)
	}()

	// Example start.
	// Store payout info in database only if payout
	// was successfully processed.

	// Initialize repository with *sql.DB.
	repo := PayoutRepository[*sql.DB]{
		sql: pgDocker.DB,
	}

	ctx := context.Background()
	p := Payout{
		Currency: "EUR",
		Amount:   15000,
	}

	if err := repo.Txed(
		ctx,
		doPayout(&p),
	); err != nil {
		log.Print(err)
	}
}

func doPayout(p *Payout) PayoutTxBlock {
	f := func(
		ctx context.Context,
		// Inside Txed call PayoutRepository will be initialized using *sql.Tx
		tx *PayoutRepository[*sql.Tx],
	) error {
		if err := tx.Store(ctx, p); err != nil {
			return fmt.Errorf("store payout: %w", err)
		}

		if err := processPayout(ctx, p); err != nil {
			return fmt.Errorf("process payout: %w", err)
		}

		return nil
	}

	return f
}

func processPayout(ctx context.Context, p *Payout) error {
	return errors.New("not implemented")
}
